module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: '黄泽斯·Daes Huang',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Daes个人博客,技术博客,笔记,记录技术日志,技术分享,前端简历博客,关于Daeshuang一些分享教程' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://unpkg.com/element-ui/lib/theme-chalk/index.css' }
    ]
  },
  css: [
    '@/assets/font/iconfont.css',
    '@/assets/global.css'
  ],
  plugins: [
    { src: '~/plugins/ElementUI', ssr: true },
    { src: '~/plugins/Particles', ssr: false },
    { src: '~/plugins/backTop', ssr: false },
    //'~/plugins/axios',
  ],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
  ],
  //proxy: {
  //  '/api/': {
  //    target: 'http://localhost:8080/',
  //    pathRewrite: {
  //      '^/api/': ''
  //    }
  //  }
  //},
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#0084ff' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    transpile: [/^element-ui/],
  }
}

