export default window.onload = function () {
  var btn = document.getElementById("back-top");
  var timer = null;
  window.onscroll = function () {
    var scroll_top = document.documentElement.scrollTop || document.body.scrollTop; //网页被卷去的高
    var scrollheight = document.body.scrollHeight; //页高

    if (scroll_top < 600) {
      btn.style.display = "none"
    } else {
      btn.style.display = "block"
    }
  }
  btn.onclick = function () {
    var scroll_top = document.documentElement.scrollTop || document.body.scrollTop;
    var scrollheight = document.body.scrollHeight; //页高
    clearInterval(timer)
    if (scroll_top > 300) {
      timer = setInterval(function () {
        var speed = (0 - scroll_top) / 8;
        speed = speed > 0 ? Math.ceil(speed) : Math.floor(speed);
        scroll_top = scroll_top + speed;
        document.documentElement.scrollTop = scroll_top;
        if (scroll_top <= 0) {
          clearInterval(timer) // 清楚定时器
        }
      }, 20)
    }
  }
}
