import Vue from 'vue'

import Element from 'element-ui'
import Locale from 'element-ui/lib/locale/lang/zh-CN'

Vue.use(Element, { Locale })
